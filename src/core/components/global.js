import styled from 'styled-components';

export const Input = styled.input`
  padding: ${props => (props.type !== 'radio' ? '5px' : '')};
  margin: ${props => (props.type !== 'radio' ? '3px 0' : '0 auto')};
  margin-top: ${props => (props.type === 'radio' ? '5px' : '')};
  width: 100%;
`;

export const Section = styled.section`
  width: 100%;
  height: auto;
  padding-top: 30px;
`;

export const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  width: 250px;
  margin: 0 auto;
`;

export const Error = styled.span`
  display: inline-block;
  font-size: 11px;
  min-height: 24px;
  color: red;
  margin: 2px 0;
`;

export const Li = styled.li`
  padding: 7px 0;
  list-style-type: none;
  width: 100%;
  border-bottom: 1px solid #eee;
  cursor: ${props => (props.error ? '' : 'pointer')};
  word-break: break-word;
  background-color: ${props => props.selected && '#eee'};

  &:hover {
    background: ${props => props.hover && props.hover}
  }
`;

export const Button = styled.button`
  display: flex;
  justify-content: center;
  align-items: center;
  padding: 0 20px;
  min-width: 244px;
  height: 40px;
  margin-bottom: 30px;
  font-family: Poppins-Medium;
  font-size: 16px;
  color: #fff;
  border-radius: 5px;
  line-height: 1.2;
  border: none;
  background-color: #0071f0;
  cursor: pointer;
`;

export const List = styled.ul`
  display: flex;
  flex-direction: column;
  position: absolute;
  width: 250px;
  text-align: center;
  z-index: 20;
  background: #fff;
  border-left: 1px solid #eee;
  border-right: 1px solid #eee;
`;

export const Subtitle = styled.h3`
  font-size: 16px;
  font-weight: 400;
  margin-bottom: 5px;
`;

export const Label = styled.label`
  display: flex;
  flex-direction: column;
  margin-bottom: 0px;
`;

export const RequiredFlag = styled.span`
  position: relative;
  &:after {
    content: '*';
    color: red;
  }
`;
