import React from 'react';

import {
  Label,
  RequiredFlag,
  Error
} from '../global'; 

function DropDown (props) {
  const { 
    options, 
    onChange, 
    name, 
    defaultValue,
    error = ''
  } = props;

  return (
    <Label>
      <RequiredFlag>{name}</RequiredFlag>
      <select
        defaultValue={defaultValue}
        name={name.toLowerCase()}
        onChange={onChange}
      >
        <option value={defaultValue} disabled hidden>
          {defaultValue}
        </option>
        
        {options.map((option, i) => (
          <option value={option} key={i}>
            {option}
          </option>
        ))}
      </select>

      <Error>{error && error}</Error>
    </Label>
  );
}

export default DropDown;
