import React from 'react';

import {
  Label,
  RequiredFlag,
  Input,
  Error
} from '../global';

function Field(props) {
  const {
    type,
    placeholder,
    name,
    onChange,
    value,
    isRequired,
    title,
    error = '',
    render
  } = props;

  return (
    <Label>
      {isRequired ? <RequiredFlag>{title}</RequiredFlag> : <span>{title}</span>}

      {render && render()}
      {!render && <Input
        type={type}
        placeholder={placeholder}
        name={name}
        onChange={onChange}
        value={value}
        autocomplete='off'
      />}

      <Error>{error}</Error>
    </Label>
  );
}

export default Field;
