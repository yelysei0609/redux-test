import React from 'react';


import {
  Input,
  Error,
  Label,
  RequiredFlag
} from 'core/components/global';
const InputComponent = props => {
  const {
    placeholder,
    error,
    required
  } = props;
  

  console.log(props, 'propss');

  return (
    <div>
      <Label>
        {required ? (
          <RequiredFlag>{placeholder}</RequiredFlag>
        ) : (
          <span>{placeholder}</span>
        )}
        <Input {...props.input} {...props}  value={props.data} />
      </Label>

      <Error>{error}</Error>
    </div>
  );
}

export default InputComponent;