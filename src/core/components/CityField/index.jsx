import React, { Component } from 'react';
import { Field } from 'redux-form';

import mock from './mock.json';
import { List, Li } from 'core/components/global';
import InputComponent from 'core/components/Input';

class CityField extends Component {
  state = {};

  getCities = () =>
      // fetch.get(`https://maps.googleapis.com/maps/api/place/autocomplete/json?input=${input}&&types=(cities)&key=AIzaSyBwhMV3GWnFZdW1W0Mad3a96tgifcvAD5E`)
      // .then(res=>res.json())
      // .then(data => this.setState({ data, showList: true }))

    Promise.resolve(mock).then(data => {
      let { city } = this.state;

      if (city === '') {
       return this.setState({ showList: false })
      }

      city = city.charAt(0).toUpperCase() + city.substr(1);

      const filteredCities = data.predictions.filter(cityName =>
        cityName.structured_formatting.main_text.startsWith(
          city
        )
      );

      this.setState({ data: filteredCities, showList: true });
    });

  onChange = e => {
    const { value: city } = e.target;
    
    this.setState({ city, showList: false });
    clearInterval(this.interval);
    this.timeoutID = setTimeout(this.getCities, this.props.debounce || 0, city);
  };

  onMouseDown = city => this.setState({ city, showList:false });

  onBlur = () => this.setState({ showList: false })

  onFocus = () => this.setState({ showList: true });

  render() {
    const { city = '', data = [], showList } = this.state;

    return (
      <>
        <Field
          name='city'
          component={InputComponent}
          type='text'
          required
          placeholder='City'
          data={city}
          onChange={this.onChange}
          onBlur={this.onBlur}
          onFocus={this.onFocus}
        />
        {showList && (
          <List>
            {data.map(el => (
              <Li
                key={el.description}
                name='city'
                onMouseDown={() => this.onMouseDown(el.description)}
                hover='#eee'
              >
                {el.structured_formatting.main_text}
              </Li>
            ))}
          </List>
        )}
      </>
    );
  }
}

export default CityField;
