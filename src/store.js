import {
  createStore, 
  applyMiddleware, 
  combineReducers 
} from 'redux';
import logger from 'redux-logger';
import createSagaMiddleware from 'redux-saga';
import { reducer as formReducer } from 'redux-form';
import rootSaga from 'sagas';

import list from 'reducers/List';

const sagaMiddleware = createSagaMiddleware();

const reducers = {
  list,
  form: formReducer
};
const rootReducer = combineReducers(reducers);
const Store = createStore(rootReducer, applyMiddleware(logger, sagaMiddleware));

sagaMiddleware.run(rootSaga);

export default Store;
