export const GENDER_OPTIONS = ['Male', 'Female', 'Other'];
export const ROLE_OPTIONS = ['Admin', 'User', 'Guest', 'TeamLead'];
export const DEPARTMENT_OPTIONS = ['JS', 'Test', 'HR', 'Design'];
