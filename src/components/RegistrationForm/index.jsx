import React from 'react';
import { Field, reduxForm } from 'redux-form';

// import schema from './schema'
import CityField from 'core/components/CityField';
import { 
  Button, 
  Label, 
  Section,
  Wrapper,
} from 'core/components/global';

import InputComponent from 'core/components/Input';
import { Gender, GenderTitle } from './style';

const RegistrationForm = props => {
  const {
    handleSubmit, 
    pristine, 
    submitting,
    options
  } = props;
  const {
    ROLE_OPTIONS,
    DEPARTMENT_OPTIONS
  } = options;

  return (
    <Section>
      <Wrapper>
      <form onSubmit={handleSubmit}>
        <Field
          name='firstName'
          component={InputComponent}
          type='text'
          required
          placeholder='First Name'
        />

        <Field
          name='lastName'
          component={InputComponent}
          type='text'
          placeholder='Last Name'
        />

        <div>
          <GenderTitle>Gender</GenderTitle>
          <Gender>
            <Field
              name='gender'
              component={InputComponent}
              type='radio'
              value='male'
              placeholder='Male'
            />

            <Field
              name='gender'
              component={InputComponent}
              type='radio'
              value='female'
              placeholder='Female'
            />

            <Field
              name='gender'
              component={InputComponent}
              type='radio'
              placeholder='Other'
              value='other'
            />
          </Gender>
        </div>

        <Field
          name='email'
          component={InputComponent}
          type='text'
          required
          placeholder='Email'
        />

        <Field
          name='password'
          component={InputComponent}
          type='text'
          required
          placeholder='Password'
        />

        <Field
          name='repeatPassword'
          component={InputComponent}
          type='text'
          required
          placeholder='Repeat password'
        />

        <CityField
          debounce={500}
          required
        />

        <div>
          <Label>
            Role
            <Field name='role' component='select'>
              <option value='' disabled hidden>
                Select your role
              </option>
              {ROLE_OPTIONS &&
                ROLE_OPTIONS.map((role, i) => (
                  <option value={role} key={i}>
                    {role}
                  </option>
                ))}
            </Field>
          </Label>
        </div>

        <div>
          <Label>
            Department
            <Field name='department' component='select'>
              <option value='' disabled hidden>
                Select your department
              </option>
              {DEPARTMENT_OPTIONS &&
                DEPARTMENT_OPTIONS.map((department, i) => (
                  <option value={department} key={i}>
                    {department}
                  </option>
                ))}
            </Field>
          </Label>
        </div>

        <Field
          component={Button}
          type='submit'
          disabled={pristine || submitting}
        >
          Submit
        </Field>
      </form>
      </Wrapper>
    </Section>
  );
}

export default reduxForm({
  form: 'registration'
})(RegistrationForm);
