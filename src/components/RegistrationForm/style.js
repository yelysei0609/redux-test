import styled from 'styled-components';
import { Subtitle } from 'core/components/global';

export const Section = styled.section`
  width: 100%;
  height: auto;
  padding-top: 30px;
`;

export const Title = styled.h2`
  font-size: 24px;
  font-weight: bold;
  text-align: center;
  margin-bottom: 25px;
`;

export const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  width: 250px;
  margin: 0 auto;
`;

export const Form = styled.form`
  display: flex;
  flex-direction: column;
`;

export const GenderTitle = styled(Subtitle)`
  text-align: center;
  font-size: 18px;
  margin-bottom: 25px;
`;

export const Gender = styled.div`
  display: flex;
  justify-content: space-between;
`;
