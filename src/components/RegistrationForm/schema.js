import Joi from '@hapi/joi';

import { ROLE_OPTIONS, DEPARTMENT_OPTIONS } from 'constants/index';

const schema = Joi.object({
  name: Joi.string()
    .required()
    .min(3)
    .max(15)
    .messages({
      'any.required': `Please enter your name`,
      'string.min': `Name can't be less than 3 characters`,
      'string.max': `Name can't be more than 15 characters`
    }),
  lastName: Joi.string()
    .min(3)
    .max(15)
    .messages({
      'any.required': 'Please enter your last name',
      'string.min': `Last name can't be less than 3 characters`,
      'string.max': `Last name can't be more than 15 characters`
    }),
  email: Joi.string()
    .required()
    .email({
      minDomainSegments: 2,
      tlds: { allow: ['com', 'net', 'io', 'ua'] }
    })
    .messages({
      'any.required': 'Please enter your email',
      'string.email': 'Enter correct email!'
    }),
  password: Joi.string()
    .required()
    .pattern(new RegExp('^[a-zA-Z0-9]{3,30}'))
    .messages({
      'string.pattern.base': `Minimum length 3 characters, maximum 30, 'a-z', 'A-Z', '0-9'`,
      'any.required': 'Please enter your password'
    }),
  repeatPassword: Joi.string()
    .valid(Joi.ref('password'))
    .required()
    .messages({
      'any.only': 'Repeated password must match the password!',
      'any.required': 'Please repeat your password'
    }),
  role: Joi.string()
    .required()
    .valid(...ROLE_OPTIONS)
    .messages({
      'any.required': 'Please select your role'
    }),
  department: Joi.string()
    .required()
    .valid(...DEPARTMENT_OPTIONS)
    .messages({
      'any.required': 'Please select your department'
    }),
  city: Joi.string()
  .required()
  .messages({
    'any.required': 'Please enter your city'
  })
});

export default schema;
