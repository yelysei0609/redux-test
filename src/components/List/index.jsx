import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  string, 
  object,
  bool,
  arrayOf,
} from 'prop-types';

import { listItemClick, loadListRequest } from 'actions/List';
import { Li } from 'core/components/global';
import { Wrapper } from './style';

const mapStateToProps = state => {
  const {
    data, 
    selected, 
    loading,
    error
  } = state.list;

  return {
    data, 
    selected,
    loading,
    error
  };
};

const mapDispatchToProps = { listItemClick, loadListRequest };

class List extends Component {

  componentDidMount() {
    this.props.loadListRequest();
  }

  render() {
    const { 
      selected, 
      data,
      loading,
      error
    } = this.props;

    return (
      <Wrapper>
        {loading ? (
          <h2>Loading...</h2>
        ) : (
          data.map(name => (
            <Li
              key={name}
              onClick={e => this.props.listItemClick(e.target.textContent)}
              selected={selected === name}
            >
              {name}
            </Li>
          ))
        )}
        {error && <Li error>{error.message}</Li>}
      </Wrapper>
    );
  }
}

List.propTypes = {
  selected: string,
  loading: bool,
  error: object,
  data: arrayOf(string)
}

export default connect(mapStateToProps, mapDispatchToProps)(List);
