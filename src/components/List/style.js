import styled from 'styled-components';

export const Wrapper = styled.ul`
  padding: 0;
  display: flex;
  flex-direction: column;
  width: 100%;
  border: 2px solid #eee;
  text-align: center;
  width: 225px;
  height: 100%;
`;
