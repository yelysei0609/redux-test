import {
  LOAD_LIST_REQUEST, 
  LOAD_LIST_SUCCESS,
  LOAD_LIST_FAILURE,
  LIST_ITEM_CLICK 
} from 'actions/List';

const initState = {
  data: [],
  loading: false,
  selected: null,
  error: null
};


const list = (state = initState, action) => {
  const { payload } = action;

  switch(action.type) {
    case LOAD_LIST_REQUEST:
      return { ...state, loading:true };

    case LOAD_LIST_SUCCESS: 
      return { ...state, data: payload, loading: false };

    case LOAD_LIST_FAILURE:
      return { ...state, error: payload, loading: false }
    
    case LIST_ITEM_CLICK:
      return { ...state, selected: payload };
    
    default : return state;
  }
}

export default list;
