class Api {
  getList = () =>
    fetch('http://localhost:8000/list', {
      method: 'GET'
    }).then(res => res.json());
}

export default new Api();
