import React, { Component } from 'react';
import { connect } from 'react-redux';

import { GENDER_OPTIONS, ROLE_OPTIONS, DEPARTMENT_OPTIONS } from 'constants/index';
import RegistrationForm from 'components/RegistrationForm'
import List from 'components/List';
import { Container } from './style';


class App extends Component {
  onSubmitRegistration = data => {
    
    console.log(data);
  };

  render() {
    return (
      <Container>
        <aside>
          <List />
        </aside>

        <RegistrationForm handleSubmit={this.onSubmitRegistration}
          options={{ GENDER_OPTIONS, ROLE_OPTIONS, DEPARTMENT_OPTIONS }}
          InputRef={this.input}
          />
      </Container>
    );
  }
}

export default connect()(App);
