import { call, put, takeLatest } from 'redux-saga/effects';

import {
  loadListSuccess,
  loadListFailure,
  LOAD_LIST_REQUEST
} from 'actions/List';

import api from 'api';

function* listRequestSaga() {
  try {
    const data = yield call(api.getList);
    yield put(loadListSuccess(data));
  } catch (error) {
    yield put(loadListFailure(error));
  }
}

function* listSaga() {
  yield takeLatest(LOAD_LIST_REQUEST, listRequestSaga);
}

export default listSaga;
