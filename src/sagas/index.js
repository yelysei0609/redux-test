import { fork, all } from 'redux-saga/effects';

import listSaga from './list.saga';

function* rootSaga() {
  yield all([fork(listSaga)]);
}

export default rootSaga;
