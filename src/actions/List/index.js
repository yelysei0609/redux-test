export const LIST_ITEM_CLICK = 'list.item.clicked';
export const LOAD_LIST_SUCCESS = 'load.list.success';
export const LOAD_LIST_REQUEST = 'load.list.request';
export const LOAD_LIST_FAILURE = 'load.list.failure';

export const loadListRequest = () => ({
  type: LOAD_LIST_REQUEST
});

export const loadListSuccess = payload => ({
  type: LOAD_LIST_SUCCESS,
  payload
});

export const loadListFailure = payload => ({
  type: LOAD_LIST_FAILURE,
  payload
});

export const listItemClick = payload => ({
  type: LIST_ITEM_CLICK,
  payload
});
